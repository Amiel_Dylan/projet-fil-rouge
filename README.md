# Projet Fil Rouge

<img src="https://img.shields.io/badge/Python-flask--uwsgi-blue" alt="made with python"> <img src="https://img.shields.io/badge/AWS-Rekognition--Comprehend--EC2--S3--Route 53-orange" alt="made with python">

Description des routes développées pour l'API conçu lors du Projet Fil Rouge dont le propos applicatif est d'accepter le dépôt de tout type de fichier (.txt, .csv, .pdf, .jpg, .png, .gif, .xlsx) et de le restituer au format JSON. L'accès au service nécessite une authentification.

---

![.png](resume.png "Architecture")
