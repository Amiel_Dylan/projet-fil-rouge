import unittest   # The test framework
from Tools import utilities
from pathlib import Path 
import sqlite3
import app

@decorators.parameters_accepted(int, (int,float))
def test(arg1, arg2):
  return arg1 * arg2

class Test_Utilities(unittest.TestCase):
    def test_is_an_allowed_file(self):  
        """Test utilities.is_an_allowed_file

        utilities.is_an_allowed_file() return a boolean (True or False) according to the
        extension of the object. True if it's allowed, False otherwise"""

        self.assertEqual(utilities.is_an_allowed_file("test.jpg"), True)

        self.assertEqual(utilities.is_an_allowed_file("test.gif"), False)

        self.assertEqual(utilities.is_an_allowed_file(" "), False)

        self.assertEqual(utilities.is_an_allowed_file(1), False)

    def test_getImageInfos(self):
        """Test utilities.getImageInfos

        utilities.getImageInfos() should raise a FileNotFoundError or TypeError if the filename is wrong
        and a tuple object if the filename is well writed"""

        with self.assertRaises(FileNotFoundError):
          utilities.getImageInfos("test")

        with self.assertRaises(TypeError):
          utilities.getImageInfos(1)

        self.assertEqual(type(utilities.getImageInfos("Tests\\image.jpg")), tuple)

    def test_clean_directory(self):
      self.assertRaises(FileNotFoundError, utilities.clean_directory, Path('./directory')) 


class Test_Flask_app(unittest.TestCase):
    def setUp(self):
        app.app.testing = True
        self.app = app.app.test_client()        

    def test_home(self):
      result = self.app.get('/')
      self.assertEqual(result.get_json()["Message"], "💻 Root page")

    def test_uploadNewImage(self):
      self.assertEqual(2, 1+1)  
      
if __name__ == '__main__':
    unittest.main()