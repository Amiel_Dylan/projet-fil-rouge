# -*- coding: utf-8 -*-
import os
from pathlib import Path 
from flask import jsonify, request
from werkzeug.utils import secure_filename
from app import app
from flask_basicauth import BasicAuth
from app.Tools import utilities

# SETUP FOR AUTHENTICATION
basic_auth = BasicAuth(app)

# ROUTES
# ================== / ===========================
@app.route('/', methods=['GET'])
@basic_auth.required
def home():
    body = {}
    body["Message"] = "🍟 Root page 🍟"
    return jsonify(body)

# ================== /upload =====================
@app.route('/upload', methods=['POST'])
@basic_auth.required
def uploadNewImage():
    body = {}

    # check if the post request has the file part
    if 'file' not in request.files:
        body["Message"] = "No file part"
        return jsonify(body)

    file = request.files['file']
    # if user does not select file, browser also
    # submit an empty part without filename
    
    if file.filename == '':
        body["Message"] = "No selected file"
        return jsonify(body)

    if file and utilities.is_an_allowed_file(file.filename):
        # Basic filename
        filename = secure_filename(file.filename)
        
        # Filename with path to temp directory
        final_filename = os.path.join(f'{utilities.UPLOAD_FOLDER}', filename)
        
        # Save file to temp directory before processing
        file.save(final_filename)

        # Get dicyt with processing result
        infos = utilities.getFileInfos(final_filename, request)
        
        # Updating result body
        body.update(infos)

        # Clean temp repertory
        utilities.clean_directory(utilities.UPLOAD_FOLDER)

        return body
    else:
        body["Message"] = "Sorry, This kind of file is not allowed ⚠️"
        return body